function getParams(){
    var params = {};
    location.href.replace(/(\w+)=(\w+)/ig, (a, b, c) => (params[b] = decodeURIComponent(c)));
    return params;
}

 function toThousands(num, y){
    y = y || ''
    num = (+num || 0).toFixed(2).toString();
    var t = num.split('.');
    return y + t[0].toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,') + (t[1] ? (`.${t[1]}`) : '');
}