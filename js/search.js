var regs = [
    { key : 'ID', reg : /\d{17}[\d|x]|\d{15}/ },
    { key : 'phone', reg : /0?(13|14|15|17|18|19)[0-9]{9}/ },
    { key : 'picCode', reg :  /[\w]{4}/ },
    { key : 'smCode', reg : /\d{4}|\d{6}/ }
]
var maxValue = 60 // 倒计时开始
var time = maxValue // 初始化倒计时时间
var during = false // 处于倒计时中
$(function(){
    init()
    addEvent()
    animie()
    var verifyCode = new GVerify({ // 验证码
        id:"qrcode",    //容器的ID
        type:"blend"    //图形验证码的类型：blend-数字字母混合类型（默认）、number-纯数字、letter-纯字母
    });
    // verifyCode.refresh(); //刷新验证码

    /**
     * 初始化dom
     */
    function init(){
        regs = regs.map(function(item){ 
            return {
                key : item.key,
                el : $( '.' + item.key + '.input-val'),
                error : $( '.' + item.key + '.mc-input-error'),
                reg : item.reg
            } 
        })
    }

    /**
     * 获取验证码 操作
     */
    function getCode(){

    }

    function animie(){
        setTimeout(() => {
            $('.logo img').addClass('active')
        })
        setTimeout(() => {
            $('.start').addClass('leave')
        },1000)
    }
    //--------------------
    /**
     * 增加事件
     */
    function addEvent(){
        $('#search').on('click', function(){
            let res = checkForm()
            if(!res) return // 没通过
            console.log(res)
        })
        $('.send-code').on("click", function(){
            if(during) return
            downTime($(this))
            getCode()
        })
        $('.security-message').on('click', function(){
            window.location.href = './loan-detail.html'
        })
    }

    
    /**
     * 倒计时
     */
    function downTime(el){
        during = true
        var timer = setInterval(function(){
            if(time < 0){
                during = false
                time = maxValue
                clearInterval(timer)
                el.html('发送验证码')
                return
            }
            el.html(time-- + 's')
        }, 1000)
    }
    /**
     * 检查表单元素
     */
    function checkForm(){
        var pass = true, res = {}
        regs.forEach(function(item){
            res[item.key] = item.el.val()
            if(!item.reg.test(item.el.val())){
                pass = false
                item.error.show()
            }else{
                item.error.hide()
            }
        })
        var target = regs.find(function(item){ return item.key == 'picCode' })
        if(!verifyCode.validate(target.el.val().trim())){
            pass = false
            target.error.show()
        }
        return pass ? res : pass
    }
})