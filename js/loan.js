$(function(){
    setLoanPeople()
    setPaid()
    setRepayable()


    /**
     * 设置还款人
     */
    function setLoanPeople(){
        $('.loaner').text('韦小宝')
        $('.message-value.phone').text('13555555555')
        $('.message-value.ID').text('530133199801011121')
        $('.message-value.card').text('6211111111111')
    }

    /**
     * 设置已还款
     */
    function setPaid(data){
        var list = [
            { name : '江西银行存管总账号：', money : 500 },
            { name : '萍乡海桐公司支付宝账号：', money : 500 },
            { name : '先花信息技术（北京）有限公司：', money : 50000 },
        ]
        var block = $('.block-message-content')
        var $clone = $('.block-message-item.clone')
        block.html('')
        list.map(item => {
            var clone = $clone.clone().show() // 复制一份
            clone.find('.name').text(item.name).end()
                 .find('.amount').text(toThousands(item.amount, '¥')).end()
            block.append(clone) // 加进去
        })
    }

    /**
     * 设置应还款
     */
    function setRepayable(data){
        var list = [
            { contract : '1002399444-10 ', amount : 510, time : '2020-12-12 12:22:11' },
            { contract : '1002399444-10 ', amount : 5102, time : '2020-12-12 12:22:11' },
            { contract : '1002399444-10 ', amount : 510, time : '2020-12-12 12:22:11' },
        ]
        var link = './contract-detail.html?'
        var block = $('.block-list-content')
        var $clone = $('.block-money-item.clone')
        block.html('')
        list.map(item => {
            var clone = $clone.clone().show() // 复制一份
            clone.find('.contract').text(item.contract).end() // 设置合同
                 .find('.pay-amount').text(toThousands(item.amount)).end() // 设置金额
                 .find('.pay-time').text(item.time).end() //设置时间
                 .find('.link').prop('href', link + '?contract=' + item.contract) // 设置链接
            block.append(clone) // 加进去
        })
    }
    /**
     * 千分化金额
     */
    function toThousands(num, y){
        y = y || ''
        num = (+num || 0).toFixed(2).toString();
        var t = num.split('.');
        return y + t[0].toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,') + (t[1] ? (`.${t[1]}`) : '');
    }
})