$(function(){

    setContract()
    setAmount()
    setInterest()
    setPay()
    setList()
    setCredit()


    function setContract(){ // 设置 编号
        $('.contract-code-value').text(getParams()['contract'])
    }

    function setAmount(){ // 设置金额
        $('.pay-amount-value').text(toThousands(12333, '¥ '))
    }

    function setInterest(){ // 设置利息
        $('.interest-detail-item.contract')
        .find('.value').text(toThousands(123123, '¥ ')).end()
        .find('.during').text('');

        $('.interest-detail-item.overdue')
        .find('.value').text(toThousands(123123, '¥ ')).end()
        .find('.during').text('');

        $('.interest-detail-item.overdue-1')
        .find('.value').text(toThousands(123123, '¥ ')).end()
        .find('.during').text('');
    }


    function setPay(){ //设置已还金额
        $('.pay-haved-value').text(toThousands(123333, '¥ '))
    }

    function setList(){ // 设置列表
        var list = [
            { 
                account : '江西银行存管总账号', 
                amount : 100, 
                paytime : '2020-12-12 12:22:11',
                importTime : '2020-12-12 12:22:11'
            },
            { 
                account : '江西银行存管总账号', 
                amount : 100, 
                paytime : '2020-12-12 12:22:11',
                importTime : '2020-12-12 12:22:11'
            },
        ]
        var block = $('.pay-contain-content')
        var $clone = $('.pay-contain-item.clone')
        block.html('')
        list.map(item => {
            var clone = $clone.clone().show() // 复制一份
            clone.find('.account-name').text(item.account).end() // 设置合同
                 .find('.account-amount').text(toThousands(item.amount, '¥ ')).end() // 设置金额
                 .find('.pay-time').text(item.paytime).end() //设置时间
                 .find('.import-time').text(item.importTime)//设置时间
            block.append(clone) // 加进去
        })
    }

    function setCredit(){ // 设置信用
        $('.credit-amount-value').text(toThousands(2143213, '¥ '))
    }
})